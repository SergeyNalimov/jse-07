#### Программа                                                                                                                       "task-manager" ver 1.0.3
##### Требования к Software
###### Open JDK 11
##### Стек Технологий  
###### Apache Maven 3.6.1
##### Разработчик
###### Налимов Сергей Викторович. email - nalimov_sv@nlmk.com
##### Сборка приложения  
###### mvn clear / mvn install
##### Запуск приложения
```
jse-03>java -jar ./target/task-manager-1.0.2.jar help
```
##### Ключи запуска приложения
```
version - Display program version.
about - Display developer info.
help - Display list of terminal commands.
exit - Exit

project-create - Create new project by name
project-list - Display list of projects
project-clear - Remove all project

task-create - Create new task by name
task-list - Display list of tasks
task-clear - Remove all task
```
